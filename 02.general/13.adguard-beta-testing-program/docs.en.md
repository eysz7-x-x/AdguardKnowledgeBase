---
title: 'AdGuard beta testing program'
published: true
taxonomy:
    category:
        - docs
---

* [How to become beta tester?](#intro)
* [Nightly builds](#nightly)
* [How to report a bug](#report-a-bug)

There is&nbsp;an&nbsp;AdGuard app for virtually every major platform out there, and we&nbsp;regularly release updates for all of&nbsp;them. This would not be&nbsp;possible without beta testing and, more importantly, beta testers. Want to&nbsp;become one? Here&rsquo;s how you can do&nbsp;it.<!--more-->

<a name="intro"></a>

### How to&nbsp;become a&nbsp;beta tester

We&nbsp;do&nbsp;not have any special requirements. All you need is&nbsp;to&nbsp;use AdGuard, update to&nbsp;the beta version when it&nbsp;is&nbsp;available and report all spotted bugs. One important thing, though: you will need either [forum](https://forum.adguard.com/index.php) or&nbsp;[GitHub](https://github.com/) account (or&nbsp;both :)) to&nbsp;become our beta tester&nbsp;&mdash; so&nbsp;you might want to&nbsp;get it&nbsp;over with before going further.

In any case, every single thing you need to know is gathered on a [dedicated page](https://adguard.com/en/beta.html).

If you want to skip the description part and get right to the deal, you can submit an application [here](https://surveys.adguard.com/beta_testing_program/form.html). It will be reviewed within 1-3 working days and you'll get a reply via email.


<a name="nightly"></a>

### Nightly builds

Nightly builds are special builds available for AdGuard for Windows and AdGuard for Android, they are marked with a Greek letter "**η**" in the main menu. They are updated every three days and contain the most recent changes made by developers. If you decide to install a Nightly build, be ready to face occasional bugs and unexpected behavior (and feel free to report them). You can download Nightly builds at these links:

[setup.exe](https://static.adguard.com/windows/nightly/setup.exe) or [https://agrd.io/windows_nightly](https://agrd.io/windows_nightly) — for AdGuard for Windows;
[adguard.apk](https://static.adguard.com/android/nightly/adguard.apk) or [https://agrd.io/android_nightly](https://agrd.io/android_nightly) — for AdGuard for Android.

Note that Nightly builds downloaded via links above will not be reverted back to older version if you switch them to release or beta update channel. Also needs to be mentioned that apps for other platforms do not have Nightly builds yet, but we plan on introducing them later.

<a name="report-a-bug"></a>

### How to&nbsp;report a&nbsp;bug

Let&rsquo;s imagine the worst happened&nbsp;&mdash; you spotted a&nbsp;bug. Or&nbsp;maybe you just want to&nbsp;share your suggestion with developers? Anyway, there are several ways to&nbsp;let&nbsp;us know: 

* Go&nbsp;to [forum.adguard.com](forum.adguard.com) and post there a&nbsp;new thread (or&nbsp;find an&nbsp;existing one) in&nbsp;the beta forum [category](https://forum.adguard.com/index.php?categories/48/). Make sure to&nbsp;read [forum rules](https://forum.adguard.com/index.php?threads/14859/) first;

* Go&nbsp;to [GitHub](https://github.com/AdguardTeam/) and find a&nbsp;suitable repository. Create a&nbsp;new issue there and describe the bug/feature request in&nbsp;details;

The two methods above are highly preferable, but you can also use other, if&nbsp;needed:

* Find&nbsp;us on&nbsp;various social media, like [Facebook](https://www.facebook.com/AdguardEn/), [Reddit](https://www.reddit.com/r/Adguard/) etc. This is&nbsp;not the most convenient way to&nbsp;discuss technical matters, though.

* Contact our support team by&nbsp;sending an&nbsp;email either from in-app support tab or&nbsp;directly&nbsp;to [support@adguard.com](mailto:support@adguard.com);

### What else?

There is&nbsp;not much to&nbsp;add. You don&rsquo;t have to&nbsp;go&nbsp;out of&nbsp;your way in&nbsp;your search for bugs. You can just use AdGuard in&nbsp;a&nbsp;way you normally would, just don&rsquo;t be&nbsp;lazy if&nbsp;you encounter a&nbsp;bug and report it :) Of&nbsp;course, we&nbsp;appreciate any additional effort and/or extra forum activity etc.

From time to&nbsp;time you will receive our newsletter, informing about the latest changes in&nbsp;beta version (in&nbsp;case you miss&nbsp;it) and other important news. You can unsubscribe from it&nbsp;if&nbsp;you want&nbsp;to.

And that&rsquo;s all! If&nbsp;you feel the urge to&nbsp;participate in&nbsp;AdGuard beta testing program you now know what to&nbsp;do!
